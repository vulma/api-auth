package main

import (
	"os"
	"io/ioutil"
	"fmt"
	"log"
	"net/url"
	"net/http"
	"net/http/httputil"
	flags "github.com/jessevdk/go-flags"

	"github.com/dgrijalva/jwt-go"
	"github.com/auth0/go-jwt-middleware"
)

const USER_HEADER = "X-Forwarded-User"
const GROUPS_HEADER = "X-Forwarded-Groups"

var options struct {
    Host	string	`long:"host" description:"the IP to listen on" default:"localhost" env:"HOST"`
    Port	int		`long:"port" description:"the port to listen on for insecure connections, defaults to a random value" env:"PORT"`
    ProxyPassUrl string `long:"proxy-pass-url" description:"the URL to redirect authenticated requests" env:"PROXY_PASS_URL"`
    PubKeyFile string `long:"pub-key-file" description:"" env:"PUB_KEY_FILE"`
}

func jwtExtractMiddleware(next http.Handler) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
  	tokenString, _  := jwtmiddleware.FromAuthHeader(r)
	token, _ := jwt.Parse(tokenString, nil)
	claims, _ := token.Claims.(jwt.MapClaims)

	r.Header.Del("Authorization")
	r.Header.Add(USER_HEADER, claims["email"].(string))

    next.ServeHTTP(w, r)
  })
}

func main() {
	os.Setenv("HOST", os.Getenv("SERVICE_HOST"))
	os.Setenv("PORT", os.Getenv("SERVICE_PORT"))

	var parser = flags.NewParser(&options, flags.Default)

	if _, err := parser.Parse(); err != nil {
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			os.Exit(0)
		} else {
			os.Exit(1)
		}
	}

	pubKeyFile, err := ioutil.ReadFile(options.PubKeyFile)
    if err != nil {
        log.Fatal(err)
    }

	proxyPassUrl, err := url.Parse(options.ProxyPassUrl)
	if err != nil {
		log.Fatal(err)
	}

	jwtAuthMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		UserProperty: "email",
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
      		return jwt.ParseRSAPublicKeyFromPEM(pubKeyFile)
    	},
    	SigningMethod: jwt.SigningMethodRS256,
    })

	reverseProxy := httputil.NewSingleHostReverseProxy(proxyPassUrl)

	app := jwtAuthMiddleware.Handler(jwtExtractMiddleware(reverseProxy))

	serverString := fmt.Sprintf("%s:%d", options.Host, options.Port)

	log.Printf("Server running on %s", serverString)
	if err := http.ListenAndServe(serverString, app); err != nil {
		log.Fatal(err)
	}
}