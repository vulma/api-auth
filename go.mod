module gitlab.com/vulma/api-auth

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20170425171159-5493cabe49f7
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/jessevdk/go-flags v1.4.0
)
